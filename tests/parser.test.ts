import {
    alpha, alpha0, alpha1,
    alphaL, alphaL0, alphaL1, alphanumeric, alphanumeric0, alphanumeric1, alphaU, alphaU0, alphaU1, delimiter,
    digit, digit0, digit1, hexadecimal, hexadecimal0, hexadecimal1, hexaNumber,
    parseA,
    parseCharacter_v1, parseCharacter_v2, parseToken_v1, parseToken_v2, run, tag
} from "../src/parser";
import {file, literal} from "../src/stream";
import * as path from "path";


describe("Token parsers", () => {
    describe("should parse A character", () => {
        test("should return false with B", () => {
            expect(parseA("B")).toBeFalsy()
        })

        test("should return true with A", () => {
            expect(parseA("A")).toBeTruthy()
        })
    });

    describe("should parse given character", () => {
        test("should return false with B", () => {
            expect(parseCharacter_v1("A", "B")).toBeFalsy()
        })

        test("should return true with A", () => {
            expect(parseCharacter_v1("A", "A")).toBeTruthy()
        })
    });

    describe("should parse given token in string", () => {
        test("should return false with BB", () => {
            expect(parseCharacter_v2("A", "BB")).toBeFalsy()
        })

        test("should return true with AB", () => {
            expect(parseCharacter_v2("A", "AB")).toBeTruthy()
        })
    });

    describe("should parse given token in string", () => {
        test("should return false because string doesn't start AB", () => {
            expect(parseToken_v1("AB", "BBC")).toBeFalsy()
        })

        test("should return true because string start AB", () => {
            expect(parseToken_v1("AB", "ABC")).toBeTruthy()
        })
    });

    describe("should return an object", () => {
        test("should return OK and the rest of the feed", () => {
            let string = "ABCD";
            let expected = {
                state: true,
                remain: "CD"
            }
            let result = parseToken_v2("AB", string);
            expect(result).toEqual(expected);
        })

        test("should return KO and the whole of the feed", () => {
            let string = "BBCD";
            let expected = {
                state: false,
                remain: "BBCD"
            }
            let result = parseToken_v2("AB", string);
            expect(result).toEqual(expected);
        })
    })

    describe("should return an object with curried function", () => {
        test("should return OK and the rest of the feed", () => {
            let string = literal("ABCD");
            let parseAB = tag("AB")
            let result = run(parseAB, string);
            expect(result.unwrap().parsed).toEqual("AB");
            expect(result.unwrap().remain.asString()).toEqual("CD");
        })

        test("should return KO and the whole of the feed", () => {
            let string = literal("BBCD");
            let parseAB = tag("AB")
            let result = run(parseAB, string);
            expect(result.unwrapError().error).toEqual("Expected [AB] not found");
            expect(result.unwrapError().remain.asString()).toEqual("BBCD");
        })

        test("should return KO on empty feed", () => {
            let string = literal("");
            let parser = tag("A");
            let result = run(parser, string);
            expect(result.unwrapError().error).toEqual("Expected [A] not found");
            expect(result.unwrapError().remain.asString()).toEqual("");
        })

    })
});

describe("Specialized parsers", () => {

    describe("Digit parser", () => {

        test("should parse a single digit", () => {

            let result = run(digit(), literal("1"));
            expect(result.unwrap().parsed).toBe("1");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(digit(),literal("8"));
            expect(result.unwrap().parsed).toBe("8");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(digit(), literal("a"));
            expect(result.unwrapError().error).toBe("Expected [digit] not found");
            expect(result.unwrapError().remain.asString()).toBe("a");
        });

        test("should parse zero or more digit", () => {
            let result = run(digit0(), literal("123a9"));
            expect(result.unwrap().parsed).toEqual(["1", "2", "3"]);
            expect(result.unwrap().remain.asString()).toBe("a9");

            result = run(digit0(), literal("abc"));
            expect(result.unwrap().parsed).toEqual([]);
            expect(result.unwrap().remain.asString()).toBe("abc");
        })

        test("should parse at least one digit", () => {
            let result = run(digit1(), literal("123a9"));
            expect(result.unwrap().parsed).toEqual(["1", "2", "3"]);
            expect(result.unwrap().remain.asString()).toBe("a9");

            result = run(digit1(), literal("1a9"));
            expect(result.unwrap().parsed).toEqual(["1"]);
            expect(result.unwrap().remain.asString()).toBe("a9");

            result = run(digit1(), literal("abc"));
            expect(result.unwrapError().error).toEqual("Expected at least one digit");
            expect(result.unwrapError().remain.asString()).toBe("abc");
        })
    })

    describe("Lower alpha parser", () => {

        test("should parse a single lower case alpha", () => {

            let result = run(alphaL(), literal("a"));
            expect(result.unwrap().parsed).toBe("a");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(alphaL(), literal("f"));
            expect(result.unwrap().parsed).toBe("f");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(alphaL(), literal("1"));
            expect(result.unwrapError().error).toBe("Expected [lower case alpha] not found");
            expect(result.unwrapError().remain.asString()).toBe("1");
        });

        test("should parse zero or more lower case alpha", () => {
            let result = run(alphaL0(), literal("abc123"));
            expect(result.unwrap().parsed).toEqual(["a", "b", "c"]);
            expect(result.unwrap().remain.asString()).toBe("123");

            result = run(alphaL0(), literal("123"));
            expect(result.unwrap().parsed).toEqual([]);
            expect(result.unwrap().remain.asString()).toBe("123");
        })

        test("should parse at least one lower case alpha", () => {
            let result = run(alphaL1(),literal("abz7a"));
            expect(result.unwrap().parsed).toEqual(["a", "b", "z"]);
            expect(result.unwrap().remain.asString()).toBe("7a");

            result = run(alphaL1(), literal("b29"));
            expect(result.unwrap().parsed).toEqual(["b"]);
            expect(result.unwrap().remain.asString()).toBe("29");

            result = run(alphaL1(), literal("123"));
            expect(result.unwrapError().error).toEqual("Expected at least one lower case alpha");
            expect(result.unwrapError().remain.asString()).toBe("123");
        })
    })

    describe("Upper case alpha parser", () => {

        test("should parse a single upper case alpha", () => {

            let result = run(alphaU(), literal("A"));
            expect(result.unwrap().parsed).toBe("A");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(alphaU(), literal("F"));
            expect(result.unwrap().parsed).toBe("F");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(alphaU(), literal("1"));
            expect(result.unwrapError().error).toBe("Expected [upper case alpha] not found");
            expect(result.unwrapError().remain.asString()).toBe("1");

            result = run(alphaU(), literal("a"));
            expect(result.unwrapError().error).toBe("Expected [upper case alpha] not found");
            expect(result.unwrapError().remain.asString()).toBe("a");
        });

        test("should parse zero or more upper case alpha", () => {
            let result = run(alphaU0(), literal("ABC123"));
            expect(result.unwrap().parsed).toEqual(["A", "B", "C"]);
            expect(result.unwrap().remain.asString()).toBe("123");

            result = run(alphaU0(), literal("123"));
            expect(result.unwrap().parsed).toEqual([]);
            expect(result.unwrap().remain.asString()).toBe("123");
        })

        test("should parse at least one upper case alpha", () => {
            let result = run(alphaU1(), literal("ABZ7a"));
            expect(result.unwrap().parsed).toEqual(["A", "B", "Z"]);
            expect(result.unwrap().remain.asString()).toBe("7a");

            result = run(alphaU1(), literal("B29"));
            expect(result.unwrap().parsed).toEqual(["B"]);
            expect(result.unwrap().remain.asString()).toBe("29");

            result = run(alphaU1(), literal("123"));
            expect(result.unwrapError().error).toEqual("Expected at least one upper case alpha");
            expect(result.unwrapError().remain.asString()).toBe("123");
        })
    })

    describe("Alpha parser", () => {

        test("should parse a single alpha", () => {

            let result = run(alpha(), literal("A"));
            expect(result.unwrap().parsed).toBe("A");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(alpha(), literal("f"));
            expect(result.unwrap().parsed).toBe("f");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(alpha(), literal("1"));
            expect(result.unwrapError().error).toBe("Expected [alpha] not found");
            expect(result.unwrapError().remain.asString()).toBe("1");

        });

        test("should parse zero or more alpha", () => {
            let result = run(alpha0(), literal("AbC123"));
            expect(result.unwrap().parsed).toEqual(["A", "b", "C"]);
            expect(result.unwrap().remain.asString()).toBe("123");

            result = run(alpha0(), literal("123"));
            expect(result.unwrap().parsed).toEqual([]);
            expect(result.unwrap().remain.asString()).toBe("123");
        })

        test("should parse at least one alpha", () => {
            let result = run(alpha1(), literal("AbZ7y"));
            expect(result.unwrap().parsed).toEqual(["A", "b", "Z"]);
            expect(result.unwrap().remain.asString()).toBe("7y");

            result = run(alpha1(), literal("B29"));
            expect(result.unwrap().parsed).toEqual(["B"]);
            expect(result.unwrap().remain.asString()).toBe("29");

            result = run(alpha1(), literal("123"));
            expect(result.unwrapError().error).toEqual("Expected at least one alpha");
            expect(result.unwrapError().remain.asString()).toBe("123");
        })
    })

    describe("Alphanumeric parser", () => {

        test("should parse a single alphanumeric", () => {

            let result = run(alphanumeric(), literal("A"));
            expect(result.unwrap().parsed).toBe("A");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(alphanumeric(), literal("f"));
            expect(result.unwrap().parsed).toBe("f");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(alphanumeric(), literal("1"));
            expect(result.unwrap().parsed).toBe("1");
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(alphanumeric(), literal("$"));
            expect(result.unwrapError().error).toBe("Expected [alphanumeric] not found");
            expect(result.unwrapError().remain.asString()).toBe("$");

        });

        test("should parse zero or more alphanumeric", () => {
            let result = run(alphanumeric0(), literal("AbC12$3"));
            expect(result.unwrap().parsed).toEqual(["A", "b", "C", "1", "2"]);
            expect(result.unwrap().remain.asString()).toBe("$3");

            result = run(alphanumeric0(), literal("$$$"));
            expect(result.unwrap().parsed).toEqual([]);
            expect(result.unwrap().remain.asString()).toBe("$$$");
        })

        test("should parse at least one alphanumeric", () => {

            let result = run(alphanumeric1(), literal("Bz2$9"));
            expect(result.unwrap().parsed).toEqual(["B", "z", "2"]);
            expect(result.unwrap().remain.asString()).toBe("$9");

            result = run(alphanumeric1(), literal("$$$"));
            expect(result.unwrapError().error).toEqual("Expected at least one alphanumeric");
            expect(result.unwrapError().remain.asString()).toBe("$$$");
        })
    })

    describe("Hexadecimal parser", () => {

        test("should parse a single hexadecimal", () => {
            let result = run(hexadecimal(), literal("a1"))
            expect(result.unwrap().parsed).toEqual("a");
            expect(result.unwrap().remain.asString()).toBe("1");

            result = run(hexadecimal(), literal("A1"))
            expect(result.unwrap().parsed).toEqual("A");
            expect(result.unwrap().remain.asString()).toBe("1");

            result = run(hexadecimal(), literal("8A1"))
            expect(result.unwrap().parsed).toEqual("8");
            expect(result.unwrap().remain.asString()).toBe("A1");

            result = run(hexadecimal(), literal("xA1"))
            expect(result.unwrapError().error).toEqual("Expected [hexadecimal digit] not found");
            expect(result.unwrapError().remain.asString()).toBe("xA1");
        })

        test("should parse zero or more hexadecimal", () => {
            let result = run(hexadecimal0(), literal("a1"))
            expect(result.unwrap().parsed).toEqual(["a", "1"]);
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(hexadecimal0(), literal("A1"))
            expect(result.unwrap().parsed).toEqual(["A", "1"]);
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(hexadecimal0(), literal("8A1"))
            expect(result.unwrap().parsed).toEqual(["8", "A", "1"]);
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(hexadecimal0(), literal("xA1"))
            expect(result.unwrap().parsed).toEqual([]);
            expect(result.unwrap().remain.asString()).toBe("xA1");
        })

        test("should parse at least one hexadecimal", () => {
            let result = run(hexadecimal1(), literal("a1"))
            expect(result.unwrap().parsed).toEqual(["a", "1"]);
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(hexadecimal1(), literal("A1"))
            expect(result.unwrap().parsed).toEqual(["A", "1"]);
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(hexadecimal1(), literal("8A1"))
            expect(result.unwrap().parsed).toEqual(["8", "A", "1"]);
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(hexadecimal1(), literal("xA1"))
            expect(result.unwrapError().error).toEqual("Expected at least one alphanumeric");
            expect(result.unwrapError().remain.asString()).toBe("xA1");
        })

        test("should parse an hexadecimal number", () => {
            let result = run(hexaNumber(), literal("#1fA23A"));
            expect(result.unwrap().parsed).toEqual(["#", ["1", "f", "A", "2", "3", "A"]]);
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(hexaNumber(), literal("0x1fA23A"));
            expect(result.unwrap().parsed).toEqual(["0x", ["1", "f", "A", "2", "3", "A"]]);
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(hexaNumber(), literal("0x1fA8b"));
            expect(result.unwrap().parsed).toEqual(["0x", ["1", "f", "A", "8", "b"]]);
            expect(result.unwrap().remain.asString()).toBe("");

            result = run(hexaNumber(), literal("0x1fA8sb"));
            expect(result.unwrap().parsed).toEqual(["0x", ["1", "f", "A", "8"]]);
            expect(result.unwrap().remain.asString()).toBe("sb");

            result = run(hexaNumber(), literal("0#1fA8sb"));
            expect(result.unwrapError().error).toEqual("Expected [hexadecimal number] not found");
            expect(result.unwrapError().remain.asString()).toBe("0#1fA8sb");
        })
    })
})

describe("Group parsers", () => {

    test("should parse a delimited group", () => {
        let string = "( toto ) tati";
        let result = run(delimiter(tag('('), tag(')')), literal(string));
        expect(result.unwrap().parsed).toEqual(" toto ".split(''))
        expect(result.unwrap().remain.asString()).toBe(" tati");
    })

    test("should parse a nested delimited group", () => {
        let string = "( ( 4 + 2 ) / 3 ) + 5";
        let result = run(delimiter(tag('('), tag(')')), literal(string));
        expect(result.unwrap().parsed).toEqual([' ', [' ', '4', ' ', '+', ' ', '2', ' '], ' ', '/', ' ', '3', ' '])
        expect(result.unwrap().remain.asString()).toBe(" + 5");
    })

    test("should parse a multi nested delimited group", () => {
        let string = "( ( 4 + 2 ) / ( 3 - 8 ) ) + 5";
        let result = run(delimiter(tag('('), tag(')')), literal(string));
        expect(result.unwrap().parsed).toEqual([' ', [' ', '4', ' ', '+', ' ', '2', ' '], ' ', '/', ' ', [ ' ', '3', ' ', '-', ' ', '8', ' ' ], ' '])
        expect(result.unwrap().remain.asString()).toBe(" + 5");
    })

    test("should parse a multi multi nested delimited group", () => {

        // g1 : ( 5 - 8 )
        // g2 : ( 8 * g1 )
        // g3 : ( 4 + 2 + g2 )
        // g4 : ( 3 - 8 )
        // g5 : ( g3 / g4 )
        // expr : g5 + 5

        let string = "( ( 4 + 2 + ( 8 * ( 5 - 8 ) ) ) / ( 3 - 8 ) ) + 5";
        let result = run(delimiter(tag('('), tag(')')), literal(string));

        let g1 = ' 5 - 8 '.split('')
        let g4 = ' 3 - 8 '.split('')
        let g2 = [' ', '8',' ', '*', ' ', g1, ' ']
        let g3 = [' ', '4', ' ', '+', ' ', '2', ' ', '+', ' ', g2, ' ']
        let g5 = [' ', g3, ' ',  '/', ' ', g4, ' ']

        expect(result.unwrap().parsed).toEqual(g5)
        expect(result.unwrap().remain.asString()).toBe(" + 5");
    })

    test("should parse a multi multi nested delimited group from file", () => {

        // g1 : ( 5 - 8 )
        // g2 : ( 8 * g1 )
        // g3 : ( 4 + 2 + g2 )
        // g4 : ( 3 - 8 )
        // g5 : ( g3 / g4 )
        // expr : g5 + 5

        let stream = file(path.resolve("./tests/fixtures/math"))
        let result = run(delimiter(tag('('), tag(')')), stream);

        let g1 = ' 5 - 8 '.split('')
        let g4 = ' 3 - 8 '.split('')
        let g2 = [' ', '8',' ', '*', ' ', g1, ' ']
        let g3 = [' ', '4', ' ', '+', ' ', '2', ' ', '+', ' ', g2, ' ']
        let g5 = [' ', g3, ' ',  '/', ' ', g4, ' ']

        expect(result.unwrap().parsed).toEqual(g5)
        expect(result.unwrap().remain.asString()).toBe(" + 5");
    })

    test("should parse a group with two different delimiter bound length", () => {
        const block = `#block
content with something
#endblock`;

        let result = run(delimiter(tag("#block"), tag("#endblock")), literal(block));

        expect(result.unwrap().parsed).toEqual(['\n', ..."content with something".split(""), '\n']);
        expect(result.unwrap().remain.asString()).toBe("");

    })

    test("should parse a group with two different delimiter bound length nested block", () => {
        const block = `#block
content with something
#block
nested
#endblock
#endblock`;

        let result = run(delimiter(tag("#block"), tag("#endblock")), literal(block));

        expect(result.unwrap().parsed).toEqual(['\n', ..."content with something\n".split(""),['\n', ..."nested".split('') ,'\n'], '\n']);
        expect(result.unwrap().remain.asString()).toBe("");

    })

    test("should parse a group with two different delimiter bound length nested block from file", () => {

        let stream = file(path.resolve("./tests/fixtures/block"))
        let result = run(delimiter(tag("#block"), tag("#endblock")), stream);

        expect(result.unwrap().parsed).toEqual(['\n', ..."content with something\n".split(""),['\n', ..."nested".split('') ,'\n'], '\n']);
        expect(result.unwrap().remain.asString()).toBe("");

    })

    test("should fail without start delimiter at the head of feed", () => {
        let string = " toto ) tati";
        let result = run(delimiter(tag('('), tag(')')), literal(string));
        expect(result.unwrapError().error).toBe("Expected [(] not found")
        expect(result.unwrapError().remain.asString()).toBe(" toto ) tati");
    })

    test("should fail without end in feed", () => {
        let string = "( toto  tati";
        let result = run(delimiter(tag('('), tag(')')), literal(string));
        expect(result.unwrapError().error).toBe("Invalid delimited group")
        expect(result.unwrapError().remain.asString()).toBe("( toto  tati");
    })
})