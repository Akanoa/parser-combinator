import {hexadecimal, run, tag} from "../src/parser";
import {allOf, andThen, map, oneOf, oneOfLiteral, oneOrMore, orElse, repeat, zeroOrMore} from "../src/combinator";
import {maybe} from "../src/monad/maybe";
import {buildSuccess} from "../src/result";
import {literal} from "../src/stream";

describe("Combinators", () => {

    describe("should define a correct andThen behavior", () => {

        test("should return KO doesn't start with A", () => {
            let parseA = tag("A");
            let parseB = tag("B");
            let string = "BBCD";
            let parseAandB = andThen(parseA, parseB);
            let result = run(parseAandB, literal(string));
            expect(result.unwrapError().error).toEqual("Expected [A] not found");
            expect(result.unwrapError().remain.asString()).toEqual("BBCD");
        })

        test("should return KO 2nd character not a B", () => {
            let parseA = tag("A");
            let parseB = tag("B");
            let string = "AZCD";
            let parseAandB = andThen(parseA, parseB);
            let result = run(parseAandB, literal(string));
            expect(result.unwrapError().error).toEqual("Expected [B] not found");
            expect(result.unwrapError().remain.asString()).toEqual("AZCD");
        })

        test("should return OK", () => {
            let parseA = tag("A");
            let parseB = tag("B");
            let string = "ABCD";
            let parseAandB = andThen(parseA, parseB);
            let result = run(parseAandB, literal(string));
            expect(result.unwrap().parsed).toEqual(["A", "B"]);
            expect(result.unwrap().remain.asString()).toEqual("CD");
        })

        test("should return OK more complex", () => {
            let parser1 = tag("le ");
            let parser2 = tag("chat");
            let string = "le chat dans la grange";
            let parser = andThen(parser1, parser2);
            let result = run(parser, literal(string));
            expect(result.unwrap().parsed).toEqual(["le ", "chat"]);
            expect(result.unwrap().remain.asString()).toEqual(" dans la grange");
        })
    })

    describe("should define a correct orElse behavior", () => {

        test("should return KO both parser failed", () => {
            let parseA = tag("A");
            let parseB = tag("B");
            let string = "ZBC";
            let parseAorB = orElse(parseA, parseB);
            let result = run(parseAorB, literal(string));
            expect(result.unwrapError().error).toEqual("Expected [B] not found");
            expect(result.unwrapError().remain.asString()).toEqual("ZBC");
        })

        test("should return OK first parser matched", () => {
            let parseA = tag("A");
            let parseB = tag("B");
            let string = "ABC";
            let parseAorB = orElse(parseA, parseB);
            let result = run(parseAorB, literal(string));
            expect(result.unwrap().parsed).toEqual("A");
            expect(result.unwrap().remain.asString()).toEqual("BC");
        })

        test("should return OK second parser matched", () => {
            let parseA = tag("A");
            let parseB = tag("B");
            let string = "BBC";
            let parseAorB = orElse(parseA, parseB);
            let result = run(parseAorB, literal(string));
            expect(result.unwrap().parsed).toEqual("B");
            expect(result.unwrap().remain.asString()).toEqual("BC");
        })
    })

    describe("should let define combination of combined parsers", () => {

        test("should match A[D|E]", () => {
            let parseA = tag("A");
            let parseD = tag("D");
            let parseE = tag("E");
            let parseDorE = orElse(parseD, parseE);
            let parser = andThen(parseA, parseDorE);
            let string =  "ADZF";
            let result = run(parser, literal(string));
            expect(result.unwrap().parsed).toEqual(["A","D"]);
            expect(result.unwrap().remain.asString()).toEqual("ZF");

            string =  "AEZF";
            result = run(parser, literal(string));
            expect(result.unwrap().parsed).toEqual(["A","E"]);
            expect(result.unwrap().remain.asString()).toEqual("ZF");

            string =  "ABZF";
            result = run(parser, literal(string));
            expect(result.unwrapError().error).toEqual("Expected [E] not found");
            expect(result.unwrapError().remain.asString()).toEqual("ABZF");

            string =  "BDZF";
            result = run(parser, literal(string));
            expect(result.unwrapError().error).toEqual("Expected [A] not found");
            expect(result.unwrapError().remain.asString()).toEqual("BDZF");
        })
    })

    describe("should allow to combine more than two parsers [allOf] ABC", () => {

        test("must define a correct allOf parser", () => {

            let parseA = tag("A");
            let parseB = tag("B");
            let parseC = tag("C");

            let parseABC = allOf([parseA, parseB, parseC]);
            let string = "ABCDE";
            let result = run(parseABC, literal(string));
            expect(result.unwrap().parsed).toEqual([["A","B"],"C"]);
            expect(result.unwrap().remain.asString()).toEqual("DE");
        });

        test("must fail if all of allOf parser failed", () => {

            let parseA = tag("A");
            let parseB = tag("B");
            let parseC = tag("C");

            let parseABC = allOf([parseA, parseB, parseC]);
            let string = "AZCDE";
            let result = run(parseABC, literal(string));
            expect(result.unwrapError().error).toEqual("Expected [B] not found");
            expect(result.unwrapError().remain.asString()).toEqual("AZCDE");
        })

        test("should return first parser result if there is only one", () => {
            let parseA = tag("A");
            let parser = allOf([parseA]);
            let string = "AZCDE";
            let result = run(parser, literal(string));
            expect(result.unwrap().parsed).toEqual("A");
            expect(result.unwrap().remain.asString()).toEqual("ZCDE");
        })
    })

    describe("should allow to combine more than two parsers [oneOf] [A|B|C]", () => {

        test("must define a correct oneOf parser", () => {

            let parseA = tag("A");
            let parseB = tag("B");
            let parseC = tag("C");

            let parseABC = oneOf([parseA, parseB, parseC]);

            let string = "ABC";
            let result = run(parseABC, literal(string));
            expect(result.unwrap().parsed).toEqual("A");
            expect(result.unwrap().remain.asString()).toEqual("BC");

            string = "BBC";
            result = run(parseABC, literal(string));
            expect(result.unwrap().parsed).toEqual("B");
            expect(result.unwrap().remain.asString()).toEqual("BC");


            string = "CBC";
            result = run(parseABC, literal(string));
            expect(result.unwrap().parsed).toEqual("C");
            expect(result.unwrap().remain.asString()).toEqual("BC");
        });

        test("must fail if one of oneOf parser failed", () => {

            let parseA = tag("A");
            let parseB = tag("B");
            let parseC = tag("C");

            let parseABC = oneOf([parseA, parseB, parseC]);
            let string = "ZBC";
            let result = run(parseABC, literal(string));
            expect(result.unwrapError().error).toEqual("Expected [C] not found");
            expect(result.unwrapError().remain.asString()).toEqual("ZBC");
        })

        test("should fail if an empty array of parsers provided", () => {
            let parser = oneOf([]);
            let string = "this should fail";
            let result = run(parser, literal(string));
            expect(result.unwrapError().error).toEqual("No parser to combine");
            expect(result.unwrapError().remain.asString()).toEqual("this should fail");
        })
    })

    describe("should allow to combine a list of token as oneOfLiteral parser", () => {

        // [le|la] [chat|chien|vache] est dans le pré
        let parserDet = oneOfLiteral(["le", "la"]);
        let parseAnimal = oneOfLiteral(["chat", "chien", "vache"]);
        let parser = allOf([parserDet, tag(" "), parseAnimal]);

        test("should success if correct string passed to parser", () => {

            let strings = [
                ["le chat est dans le pré", [["le", " "], "chat"]],
                ["le chien est dans le pré", [["le", " "], "chien"]],
                ["la vache est dans le pré", [["la", " "], "vache"]]
            ];

            for (let [string, expected] of strings) {
                // @ts-ignore
                let result = run(parser, literal(string));
                expect(result.unwrap().parsed).toEqual(expected);
                expect(result.unwrap().remain.asString()).toEqual(" est dans le pré");
            }
        })

        test("should fail if bad string given", () => {

            let string = "le caribou est dans le pré"
            let result = run(parser, literal(string));
            expect(result.unwrapError().error).toEqual("Expected [vache] not found");
            expect(result.unwrapError().remain.asString()).toEqual("le caribou est dans le pré");
        });

        test("should create a tag parser if only one string is provided", () => {

            let parser0 = oneOfLiteral(["l'orang-outan"]);
            let string = "le caribou est dans le pré"
            let result = run(parser0, literal(string));
            expect(result.unwrapError().error).toEqual("Expected [l'orang-outan] not found");
            expect(result.unwrapError().remain.asString()).toEqual("le caribou est dans le pré");
        });

        test("should failed if a empty array is provided", () => {

            let parser0 = oneOfLiteral([]);
            let string = "le caribou est dans le pré"
            let result = run(parser0, literal(string));
            expect(result.unwrapError().error).toEqual("No token to combine");
            expect(result.unwrapError().remain.asString()).toEqual("le caribou est dans le pré");
        });

    })
})

// A{5}[D|E]{2,5}Z*U+T{,5}H{2,}
describe("Repeat", () => {

    // A{3}
    test("should match exact repetition", () => {
       let parseA = tag("A");
       let parseA3Times = repeat(parseA, maybe(), maybe(3));
       let string = "AAAABC";
       let result = run(parseA3Times, literal(string));
       expect(result.unwrap().parsed).toEqual(["A", "A", "A"]);
       expect(result.unwrap().remain.asString()).toBe("ABC");

        string = "AABC";
        result = run(parseA3Times, literal(string));
        expect(result.unwrapError().error).toEqual("Expected [A] not found");
        expect(result.unwrapError().remain.asString()).toBe("AABC");
    });

    test("should match exact repetition using flatMap", () => {
        let parseA3Times = tag("A")
            .flatMap(parser => repeat(parser, maybe(), maybe(3)));

        let string = "AAAABC";
        let result = run(parseA3Times, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("ABC");

        string = "AABC";
        result = run(parseA3Times, literal(string));
        expect(result.unwrapError().error).toEqual("Expected [A] not found");
        expect(result.unwrapError().remain.asString()).toBe("AABC");
    });

    // A{2,5}
    test("should match a range of repetition", () => {
        let parseA = tag("A");
        let parser = repeat(parseA, maybe(2), maybe(5));

        let string = "AAAAAABC";
        let result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("ABC");

        string = "AAAABC";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "AABC";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "ABC";
        result = run(parser, literal(string));
        expect(result.unwrapError().error).toEqual("Expected [A] not found");
        expect(result.unwrapError().remain.asString()).toBe("ABC");
    })

    // A{,5}
    test("should match a range of repetition with open lower bound", () => {
        let parseA = tag("A");
        let parser = repeat(parseA, maybe(), maybe(5), true);

        let string = "AAAAAABC";
        let result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("ABC");

        string = "AAAABC";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "AABC";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "ABC";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A"]);
        expect(result.unwrap().remain.asString()).toBe("BC");
    })

    //A{2,}
    test("should match an open range of repetitions", () => {
        let parseA = tag("A");
        let parser = repeat(parseA, maybe(2), maybe());

        let string = "AAAAABC";
        let result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "";
        result = run(parser, literal(string));
        expect(result.unwrapError().error).toEqual("Expected [A] not found");
        expect(result.unwrapError().remain.asString()).toBe("");
    });

    //A+
    test("should match at least one repetition", () => {
        let parseA = tag("A");
        let parser = oneOrMore(parseA);

        let string = "AAAAABC";
        let result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "ABC";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A"]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "BC";
        result = run(parser, literal(string));
        expect(result.unwrapError().error).toEqual("Expected [A] not found");
        expect(result.unwrapError().remain.asString()).toBe("BC");
    });

    // A*
    test("should match any number of repetitions", () => {
        let parseA = tag("A");
        let parser = repeat(parseA, maybe(), maybe());

        let string = "AAAAABC";
        let result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "BC";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual([]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual([]);
        expect(result.unwrap().remain.asString()).toBe("");

        string = "AAA";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("");
    })

    // zero or more
    test("should match zero or repetitions", () => {
        let parseA = tag("A");
        let parser = zeroOrMore(parseA);

        let string = "AAAAABC";
        let result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "BC";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual([]);
        expect(result.unwrap().remain.asString()).toBe("BC");

        string = "";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual([]);
        expect(result.unwrap().remain.asString()).toBe("");

        string = "AAA";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["A", "A", "A"]);
        expect(result.unwrap().remain.asString()).toBe("");
    })

    // #[[0-9A-F]{6}|[0-9A-F]{3}]
    test("should detect color", () => {

        let parseNum = oneOfLiteral(["1", "2", "5", "6"]);
        let parseLetter = oneOfLiteral(["E", "F"]);
        let parseDigit = oneOf([parseNum, parseLetter]);
        let parse3Digits = repeat(parseDigit, maybe(), maybe(3));
        let parse6Digits = repeat(parseDigit, maybe(), maybe(6));
        let parseDigits = oneOf([parse6Digits, parse3Digits])
        let parser = allOf([tag("#"), parseDigits]);

        let string = "#F1E256";
        let result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["#", ["F", "1", "E", "2", "5", "6"]]);
        expect(result.unwrap().remain.asString()).toBe("");

        string = "#F1E";
        result = run(parser, literal(string));
        expect(result.unwrap().parsed).toEqual(["#", ["F", "1", "E"]]);
        expect(result.unwrap().remain.asString()).toBe("");

        string = "#FxE";
        result = run(parser, literal(string));
        expect(result.unwrapError().error).toBe("Expected [F] not found");
        expect(result.unwrapError().remain.asString()).toBe("#FxE");
    })
})

describe("Map", () => {

    test("should apply a map on parser result", () => {
        let string = "AAAo"
        let parser = oneOrMore(tag("A"));
        let mappedParser = map(parser, (r) => {
            return r.map((val) => {
                let parsed  = val.parsed as string[];
                return buildSuccess(parsed.join(''), val.remain)
            })
        });
        let result = run(mappedParser, literal(string));
        expect(result.unwrap().parsed).toEqual("AAA");
        expect(result.unwrap().remain.asString()).toEqual("o");
    })


    test("should parse color and return its components", () => {

        interface Color {
            red: number,
            green: number,
            blue: number
        }

        const buildColor = (r: string, g: string, b: string) : Color => {
            return {
                red: parseInt(r, 16),
                green: parseInt(g, 16),
                blue: parseInt(b, 16)
            }
        }

        const map3Digits = val => {
            let [r, g, b] = val.parsed as string[];
            return buildSuccess(buildColor(r+r, g+g, b+b), val.remain)
        }

        const map6Digits = val => {
            let [r1, r2, g1, g2, b1, b2] = val.parsed as string[];
            return buildSuccess(buildColor(r1+r2, g1+g2, b1+b2), val.remain)
        }

        let parse3Digits = map(repeat(hexadecimal(), maybe(), maybe(3)), r => {
            return r.map(map3Digits)
        });
        let parse6Digits = map(repeat(hexadecimal(), maybe(), maybe(6)), r => {
            return r.map(map6Digits)
        });
        let parseDigits = oneOf([parse6Digits, parse3Digits])
        let parser = map(allOf([tag("#"), parseDigits]), r => {
          return r.map(val => buildSuccess(val.parsed[1], val.remain))
        });

        let red = "#FF0000";
        let result = run(parser, literal(red))
        expect(result.unwrap().parsed).toEqual({
            red: 255,
            green: 0,
            blue: 0
        })
        let color = "#6ab825";
        result = run(parser, literal(color))
        expect(result.unwrap().parsed).toEqual({
            red: 106,
            green: 184,
            blue: 37
        });

        color = "#fe8";
        result = run(parser, literal(color))
        expect(result.unwrap().parsed).toEqual({
            red: 255,
            green: 238,
            blue: 136
        });
    })
})
