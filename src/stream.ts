import * as fs from 'fs'
import {StringDecoder} from 'string_decoder'
import {IMaybe, maybe} from "./monad/maybe";

/**
 * The stream contract
 */
export interface IStream {
    // Returns the N characters without consume them
    take(size: number) : string,
    // Returns a new stream shifted by N characters
    tap(size: number) : IStream,
    // Return the remaining length until EOF
    length(): number,
    // Return the string representation of the stream ( should be truncated )
    asString(bufferSize ?: number): string
}

/**
 * Define contract filled by a file object
 */
interface IFile {
    fd: number,
    offset: number
}

/**
 * Build a stream from a file
 * @param {string} path -- The path the file to read
 * @param {IMaybe<IFile>} maybeFile -- Handle data related to the file
 * @return {IStream} -- The stream bound to file
 */
export const file = (path: string, maybeFile: IMaybe<IFile> = maybe()) : IStream => {

    let {fd, offset} = maybeFile.match({
        some: (fileData => fileData),
        none: () => {
            let fd = fs.openSync(path, 'r')
            let offset = 0;
            return {fd, offset}
        }
    })

    return {
        take(size: number): string {
            const decoder = new StringDecoder("utf-8")
            let complete = 0;
            let position = 0;
            let bytes = []
            while (complete !== size) {
                let buf = new Uint8Array(1)
                fs.readSync(fd, buf, 0, 1, offset+position)
                let data = buf[0]
                if (data !== 195) {
                    complete++
                }
                bytes.push(data)
                position++;
            }

            let buf = Uint8Array.from(bytes)
            return decoder.write(Buffer.from(buf))
        },
        tap(size: number): IStream {
            return file(path, maybe({fd, offset: offset + size}))
        },
        length(): number {
            return fs.statSync(path).size - offset
        },
        asString(bufferSize:number = 1024): string {
            const decoder = new StringDecoder("utf-8")
            let buffer = new Uint8Array(bufferSize)
            let num = fs.readSync(fd, buffer, 0, bufferSize, offset)
            return decoder.write(Buffer.from(buffer.slice(0, num)))
        }
    }
}
/**
 * Build a stream from a string
 * @param {string} data -- The input string
 * @return {IStream} -- The stream containing the string
 */
export const literal = (data: string) : IStream => {
    return {
        take(size: number): string {
            return data.slice(0, size)
        },
        tap(size: number): IStream {
            return literal(data.slice(size))
        },
        length(): number {
            return data.length
        },
        asString(_: number = 0): string {
            return data
        }
    }
}