export interface IResult<T, E> {
    isOk(): boolean,
    unwrap(): T | never,
    unwrapError() : E,
    mapError<E2>(fn: (err: E) => E2): IResult<T, E2>,
    map<T2>(fn: (val: T) => T2): IResult<T2, E>,
    flatMap<T2>(fn: (val: T) => IResult<T2, E>) : IResult<T2, E>
}

const unwrapValue = <T>(val: T) => () => val

export interface IResultSuccess<T, E = never> extends IResult<T, E> {
    unwrap(): T,
    unwrapError(): never,
    mapError<E2>(fn: (err: E) => E2): IResultSuccess<T>,
    map<T2>(fn: (val: T) => T2): IResultSuccess<T2>,
}

export interface IResultFailed<T, E> extends IResult<T, E> {
    unwrap() : never,
    unwrapError(): E,
    mapError<E2>(fn: (err: E) => E2): IResultFailed<never, E2>,
    map<T2>(fn: (val: T) => T2): IResultFailed<never, E>
    flatMap<T2>(fn: (val: T) => IResult<T2, E>) : IResultFailed<never, E>
}

const ok = <T, E = never>(val: T) : IResultSuccess<T, E> => {
    return {
        isOk: () => true,
        unwrap: unwrapValue(val),
        unwrapError: () => {throw new ReferenceError("Can't unwrap error on success")},
        mapError: <E2>(_: (err: E) => E2) => ok(val),
        map: <T2>(fn: (val: T) => T2) => ok(fn(val)),
        flatMap: <T2>(fn: (val: T) => IResult<T2, E>) => fn(val)
    }
}

const failed = <T, E>(err:E) : IResultFailed<T, E> => {
    return {
        isOk: () => false,
        unwrap: () => {throw new ReferenceError("Can't unwrap value on error")},
        unwrapError: unwrapValue(err),
        mapError: <E2>(fn: (err: E) => E2) => failed(fn(err)),
        map: <T2>(_: (val: T) => T2) => failed(err),
        flatMap: <T2>(_: (_: T) => IResult<T2, E>) => failed(err)
    }
}

export const result = <T, E>(state: boolean, successValue: T, errorValue: E) : IResult<T, E> => {
    return state ? ok<T, E>(successValue) : failed<T, E>(errorValue)
}