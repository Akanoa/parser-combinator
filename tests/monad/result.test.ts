import {result} from "../../src/monad/result";

describe("Map", () => {
    test("should map a success result", () => {
        let r = result(true, 12, null)
            .map(val => val.toString())
        expect(r.isOk()).toBeTruthy()
        expect(r.unwrap()).toBe("12")
    })

    test("should do nothing on failed result", () => {
        let r = result(false, null, "bad value")
            .map(val => val)
        expect(r.isOk()).toBeFalsy()
        expect(r.unwrapError()).toBe("bad value")
    })
})

describe("MapError", () => {
    test("should mapError a failed result", () => {
        let r = result(false, null, "Bad value")
            .mapError(_ => "Modified error")
        expect(r.isOk()).toBeFalsy()
        expect(r.unwrapError()).toBe("Modified error")
    })

    test("should do nothing on success result", () => {
        let r = result(true, 12, "Bad value")
            .mapError(val => val)
        expect(r.isOk()).toBeTruthy()
        expect(r.unwrap()).toBe(12)
    })
})

describe("FlatMap", () => {
    test("should map a success result", () => {
        let r = result(true, 12, null)
            .flatMap(val => result(true, val.toString(), null))
        expect(r.isOk()).toBeTruthy()
        expect(r.unwrap()).toBe("12")
    })

    test("should do nothing on failed result", () => {
        let r = result(false, null, "bad value")
            .flatMap(val => result(true, val.toString(), null))
        expect(r.isOk()).toBeFalsy()
        expect(r.unwrapError()).toBe("bad value")
    })
})