import {
    buildError, ParseError,
    ParserResult,
    ParsingResultObject,
    Result
} from "./result";
import {allOf, oneOf, oneOfLiteral, oneOrMore, zeroOrMore} from "./combinator";
import {range} from "./utils";
import {IStream} from "./stream";

export function parseA(val) :boolean {
    return val === 'A'
}

export function parseCharacter_v1(char: string, val: string) : boolean {
    return val === char
}

export function parseCharacter_v2(char: string, val: string): boolean {
    return val[0] === char
}

export function parseToken_v1(token, val): boolean {
    return val.slice(0, token.length) === token
}

export function parseToken_v2(token, val): ParsingResultObject {


    if(val.slice(0, token.length) === token) {

        let remain = val.slice(token.length);

        return {
            state: true,
            remain
        }
    }

    return {
        state: false,
        remain: val
    }
}

/**
 * @type {(string) => Result}
 */
export type InnerParser = (val: IStream) => ParserResult;

export interface Parser {
    inner: InnerParser,
    flatMap(fn: (parser: Parser) => Parser) : Parser,
    mapError(err: ParseError): Parser
}

/**
 * Build a parser from an inner function
 * @param {InnerParser} inner -- The inner function used to parse
 * @return {Parser} -- The parser
 */
export const buildParser = (inner: InnerParser) : Parser => {
    return {
        inner,
        flatMap: (fn: (parser: Parser) => Parser) => fn(buildParser(inner)),
        mapError(err: ParseError): Parser {
            const innerParser = (val: IStream) =>
                run(buildParser(inner), val)
                    .mapError(innerError => buildError(err, innerError.remain))
            return buildParser(innerParser)
        }
    }
}

/**
 * Run a parser with the provided value
 * @param {Parser} parser -- Parser to run
 * @param {string} val -- Input data
 * @return {ParserResult} -- The parser result with provided data
 */
export const run = (parser: Parser, val: IStream) : ParserResult => {
    return parser.inner(val)
}

/**
 * Build a parser which checks whether
 * input string starts with token.
 * @type {string} token
 * @return {Parser}
 */
export const tag = (token: string) : Parser => {

    const inner = (input: IStream) : ParserResult => {

        if(input.take(token.length) === token) {

            let remain = input.tap(token.length);
            return Result.ok(token, remain)
        }

        return Result.failed(`Expected [${token}] not found`, input)
    }

    return buildParser(inner)
}

/**
 * Return a parser able to parse a number digit
 * @return {ParserResult} -- The result of the parser application
 */
export const digit =  () : Parser => {

    return oneOfLiteral(range('0', '9'))
        .mapError("Expected [digit] not found")

}

/**
 * Repeat a digit parser zero or more time
 * @return {ParserResult} -- The result of the parser application
 */
export const digit0 = () : Parser => {
    return zeroOrMore(digit())
}

/**
 * Repeat a digit parser zero or more time
 * @return {Parser} -- The one or more digit parser
 */
export const digit1 = () : Parser => {
    return oneOrMore(digit())
        .mapError("Expected at least one digit")
}

/**
 * Return a parser able to parse an lower case alpha
 * @return {Parser} -- The Alpha lowercase Parser
 */
export const alphaL =  () : Parser => {

    return oneOfLiteral(range('a', 'z'))
        .mapError("Expected [lower case alpha] not found")
}

/**
 * Repeat a lower case alpha parser zero or more time
 * @return {Parser} -- The zero or more alpha lowercase parser
 */
export const alphaL0 = () : Parser => {
    return zeroOrMore(alphaL())
}

/**
 * Repeat a lower case alpha parser at least one time
 * @return {Parser} -- The one or more alpha lowercase parser
 */
export const alphaL1 = () : Parser => {
    return oneOrMore(alphaL())
        .mapError("Expected at least one lower case alpha")
}

/**
 * Return a parser able to parse a upper case alpha
 * @return {Parser} -- The Alpha uppercase Parser
 */
export const alphaU =  () : Parser => {

    return oneOfLiteral(range('A', 'Z'))
        .mapError("Expected [upper case alpha] not found")
}

/**
 * Repeat an upper case alpha parser zero or more time
 * @return {Parser} -- The zero or more alpha lowercase parser
 */
export const alphaU0 = () : Parser => {
    return zeroOrMore(alphaU())
}

/**
 * Repeat an upper case alpha at least one time
 * @return {Parser} -- The one or more alpha uppercase parser
 */
export const alphaU1 = () : Parser => {

    return oneOrMore(alphaU())
        .mapError("Expected at least one upper case alpha")
}

/**
 * Return a parser able to parse an alpha
 * @return {Parser} -- The Alpha Parser
 */
export const alpha = () : Parser => {
    return oneOf([alphaL(), alphaU()])
        .mapError("Expected [alpha] not found")
}

/**
 * Repeat an alpha parser zero or more time
 * @return {Parser} -- The zero or more alpha parser
 */
export const alpha0 = () : Parser => {
    return zeroOrMore(alpha())
}

/**
 * Repeat an alpha at least one time
 * @return {Parser} -- The one or more alpha parser
 */
export const alpha1 = () : Parser => {
    return oneOrMore(alpha())
        .mapError("Expected at least one alpha")
}

/**
 * Return a parser able to parse an alphanumeric
 * @return {Parser} -- The alphanumeric parser
 */
export const alphanumeric = () : Parser => {
    return oneOf([alpha(), digit()])
        .mapError("Expected [alphanumeric] not found")
}

/**
 * Repeat an alphanumeric parser zero or more time
 * @return {Parser} -- The zero or more alphanumeric parser
 */
export const alphanumeric0 = () : Parser => {
    return zeroOrMore(alphanumeric())
}

/**
 * Repeat an alphanumeric at least one time
 * @return {Parser} -- The one or more alphanumeric parser
 */
export const alphanumeric1 = () : Parser => {
    return oneOrMore(alphanumeric())
        .mapError("Expected at least one alphanumeric")
}

/**
 * Return a parser able to parse an hexadecimal digit
 * @return {Parser} -- The hexadecimal digit parser
 */
export const hexadecimal = () : Parser => {
    let parseLetterUpperCase = oneOfLiteral(range('A', 'F'));
    let parseLetterLowerCase = oneOfLiteral(range('a', 'f'));
    return oneOf([parseLetterLowerCase, parseLetterUpperCase, digit()])
        .mapError("Expected [hexadecimal digit] not found")
}

/**
 * Repeat an alphanumeric parser zero or more time
 * @return {Parser} -- The zero or more hexadecimal digit parser
 */
export const hexadecimal0 = () : Parser => {
    return zeroOrMore(hexadecimal())
}

/**
 * Repeat an alphanumeric at least one time
 * @return {ParserResult} -- The one or more hexadecimal digit parser
 */
export const hexadecimal1 = () : Parser => {
    return oneOrMore(hexadecimal())
        .mapError("Expected at least one alphanumeric")
}

/**
 * Parse an hexadecimal number either 0xfff or #fff or #FFF
 * @return {Parser} -- The parser able to parse a full hexadecimal number
 */
export const hexaNumber =  () : Parser => {
    let parseLetterLowerCase = oneOfLiteral(range('a', 'f'));
    let parseLetterUpperCase = oneOfLiteral(range('A', 'F'));
    let parseLetter = oneOf([parseLetterLowerCase, parseLetterUpperCase]);
    let parseHexaDigits = oneOrMore(oneOf([parseLetter, digit()]));
    let parserPrefix = oneOfLiteral(["0x", "#"]);
    let parser = allOf([parserPrefix, parseHexaDigits]);

    return parser
        .mapError("Expected [hexadecimal number] not found")
}

/**
 * Consumes input until reach of the end delimiter
 * @param {Parser} start -- Parser of start delimiter
 * @param {Parser} end -- Parser of end delimiter
 * @return {ParserResult} -- The delimited group
 */
export const delimiter = (start: Parser, end: Parser) : Parser => {
    const inner = (val: IStream) : ParserResult => {

        //first we check that input starts with start delimiter
        const resultStartDelimiter = run(start, val);
        if (!resultStartDelimiter.isOk()) {
            return resultStartDelimiter;
        }

        let parsedValues = [];
        let remain = resultStartDelimiter.unwrap().remain;
        let state = false;
        let chunk;

        // End of feed ?
        while(remain.length() !== 0) {

            let delta = 1;

            // end delimiter found
            const resultEnd = run(end, remain)
            if (resultEnd.isOk()) {
                remain = resultEnd.unwrap().remain
                state = true;
                break;
            }

            chunk = remain.take(1)

            // start delimiter found
            const resultStart = run(start, remain)
            if (resultStart.isOk()) {
                let parser = delimiter(start, end);
                let parserResult = run(parser, remain);

                if (parserResult.isOk()) {
                    let parsed = parserResult.unwrap().parsed
                    remain = parserResult.unwrap().remain;
                    chunk = parsed;
                    delta = 0
                }
            }

            parsedValues.push(chunk)
            remain = remain.tap(delta)

        }

        return state
            ? Result.ok(parsedValues, remain)
            : Result.failed("Invalid delimited group", val)
    }
    return buildParser(inner)
}