import {IResult, result} from "./monad/result";
import {IStream} from "./stream";

interface ParsingResultSuccess {
    parsed: ParsedData ,
    remain: IStream
}

interface ParsingResultFailure {
    error : string,
    remain: IStream,
}


type ParsedData = NonNullable<any>
export type ParseError = string

export type ParserResult = IResult<ParsingResultSuccess, ParsingResultFailure>

export interface ParsingResultObject {
    state: boolean,
    remain: string
}

export const buildError = (error: ParseError, remain: IStream) : ParsingResultFailure => {
    return {
        error,
        remain
    }
}

export const buildSuccess = (parsed: ParsedData, remain: IStream) : ParsingResultSuccess => {
    return {
        parsed,
        remain
    }
}

export class Result {

    static ok(parsed: ParsedData, remain: IStream) : ParserResult {

        return result(true, buildSuccess(parsed, remain), null)
    }

    static failed(error: ParseError, remain: IStream) : ParserResult {

        return result(false, null, buildError(error, remain))
    }
}