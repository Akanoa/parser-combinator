import {Result} from "../src/result";
import {literal} from "../src/stream";

describe("Result object", () => {

    test("define a KO object", () => {
        let result = Result.failed("Bad result", literal("remain string"));
        expect(result.unwrapError().error).toBe("Bad result");
        expect(result.unwrapError().remain.asString()).toBe("remain string");
        expect(() => result.unwrap()).toThrowError();
        expect(result.isOk()).toBeFalsy();
    });

    test("define a OK object", () => {

        let result = Result.ok("AAA", literal("BBB"));
        expect(result.unwrap().parsed).toBe("AAA");
        expect(result.unwrap().remain.asString()).toBe("BBB");
        expect(() => result.unwrapError()).toThrowError();
        expect(result.isOk()).toBeTruthy();
    })
});