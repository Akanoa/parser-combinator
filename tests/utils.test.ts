import {range} from "../src/utils";

describe("check range behavior", () => {

    test("alpha range", () => {
        let result = range('A', 'F')
        expect(result).toEqual(['A', 'B', 'C', 'D', 'E', 'F'])

        result = range('0', '9')
        expect(result).toEqual(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
    })
})