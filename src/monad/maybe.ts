interface IMaybeMatchPattern<T, R> {
    some(val: NonNullable<T>) : R,
    none(): R
}

export interface IMaybe<T> {
    isNone(): boolean
    match<R>(pattern: IMaybeMatchPattern<T, R>): R
}

const isEmpty = <T>(value: T) => value === null || value === undefined;
const match = <T>(value ?: T) => <R>(pattern: IMaybeMatchPattern<T, R>) => {
    return isEmpty(value) ? pattern.none() : pattern.some(value as NonNullable<T>)
}

export const maybe = <T>(value ?: T) : IMaybe<NonNullable<T>> => {
    return {
        isNone: () => isEmpty(value),
        match: match(value)
    }
}