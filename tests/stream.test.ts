import {file, literal} from "../src/stream";
import * as path from 'path'

describe("Stream from file", () => {
    test("should take the N first bytes of the file", () => {

        let stream = file(path.resolve("./tests/fixtures/a"))
        let result = stream.take(2)
        expect(result).toBe("AZ")
    })

    test("should tap the N first bytes and return a new stream shifted by N bytes", () => {

        let stream = file(path.resolve("./tests/fixtures/a"))
        let result = stream.take(2);
        expect(result).toBe("AZ");
        stream = stream.tap(2);
        let result2 = stream.take(3);
        expect(result2).toBe("BEC")
    })

    test("should return the remaining length of the stream", () => {

        let stream = file(path.resolve("./tests/fixtures/a"));
        expect(stream.length()).toBe(5)
        stream = stream.tap(2)
        expect(stream.length()).toBe(3)

    })

    test("should handle accent", () => {
        let stream = file(path.resolve("./tests/fixtures/accent"))
        let result = stream.take(4)
        expect(result).toBe("ßnoë")
    })
})

describe("Stream from string", () => {

    test("should handle accent", () => {
        let stream = literal("ßnoël");
        let result = stream.take(4)
        expect(result).toBe("ßnoë")
    })
})

// : 👩👧👦