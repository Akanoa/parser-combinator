import {ParserResult, Result} from "./result";
import {tag, Parser, buildParser, run} from "./parser";
import {IMaybe, maybe} from "./monad/maybe";
import {IStream} from "./stream";

/**
 * Combine two parser whose must both match
 * @param {Parser} parser1 First parser
 * @param {Parser} parser2 Second parser
 * @return {Parser} Combined parser
 */
export const andThen = (parser1: Parser, parser2: Parser) : Parser => {

    const inner = (val: IStream) : ParserResult => {
        // Run first parser on input
        let result1 = run(parser1, val);
        // first parser has failed
        if (!result1.isOk()) {
            return result1
        }
        // Run the second parser on remain of the first parser result
        let result2 = run(parser2, result1.unwrap().remain);
        // second parser has failed return all the input as remain
        if (!result2.isOk()) {
            return Result.failed(result2.unwrapError().error, val);
        }
        // Both has matched
        return Result.ok(
            [
                result1.unwrap().parsed,
                result2.unwrap().parsed
            ]
            , result2.unwrap().remain)
    }
    return buildParser(inner)

}

/**
 * Combine two parsers whose at least on parser must match
 * @param {Parser} parser1 First parser
 * @param {Parser} parser2 Second parser
 * @return {Parser} Combined parser
 */
export const orElse = (parser1: Parser, parser2: Parser) : Parser => {

    const inner = (val: IStream) : ParserResult => {
        // Run first parser on input
        let result1 = run(parser1, val);
        // If success
        if (result1.isOk()) {
            return result1
        }
        // Else run 2nd parser
        let result2 = run(parser2, val);
        // If success
        if (result2.isOk()) {
            return result2
        }
        // If both failed
        return Result.failed(result2.unwrapError().error, val)
    }

    return buildParser(inner)
}

type Combinator = (parser1: Parser, parser2: Parser) => Parser;

/**
 * Combine some parsers into another using a combinator
 * @param {Array<Parser>} parsers -- Parsers to combine
 * @param {Combinator} combinator -- The combinator use to build the new parser
 * @return {Parser} -- The result combined parser
 */
const combine = (parsers: Array<Parser>, combinator: Combinator) : Parser => {

    const inner = (val: IStream) : ParserResult => {

        // If empty array of parser provided
        if (parsers.length === 0) {
            return Result.failed("No parser to combine", val);
        }

        // If array of one parser provided
        if (parsers.length === 1) {
            return run(parsers[0], val)
        }

        let parser = parsers.slice(1).reduce((acc, value) => {
            return combinator(acc, value)
        }, parsers[0]);

        return run(parser, val)
    }

    return buildParser(inner)
}

const combineLiteral = (tokens: string[], combinator: Combinator) : Parser => {

    const inner = (val: IStream) : ParserResult => {

        // If empty array of parser provided
        if (tokens.length === 0) {
            return Result.failed("No token to combine",val);
        }

        // If array of one parser provided
        if (tokens.length === 1) {
            return run(tag(tokens[0]), val)
        }
        let parser = tokens.slice(1).reduce((acc: Parser, value) => {
            return combinator(acc, tag(value))
        }, tag(tokens[0]));

        return run(parser, val)
    }

    return buildParser(inner)
}

/**
 * Build a parser whose all parsers must match
 * @param {Array<Parser>} parsers  Parsers to combine if parser1 is not an array
 * @return {Parser} The result combined parser
 */
export const allOf = (parsers: Array<Parser>) : Parser => {
    const inner = (val: IStream) : ParserResult => {
        return run(combine(parsers, andThen), val)
    }
    return buildParser(inner)
}

/**
 * Build a parser whose one of parsers must match
 * @param {Array<Parser>} parsers Parsers to combine if parser1 is not an array
 * @return {Parser} The combined parser
 */
export const oneOf = (parsers: Array<Parser>) : Parser => {
    const inner = (val: IStream) : ParserResult => {
        return run(combine(parsers, orElse), val)
    }
    return buildParser(inner)
}

/**
 * Build a parsers that match one of token provided
 * @param tokens
 */
export const oneOfLiteral = (tokens: string[]) : Parser => {
    const inner =  (val: IStream) : ParserResult => {
        return run(combineLiteral(tokens, orElse), val);
    }
    return buildParser(inner)
}


/**
 * This combinator apply multiple time the same parser to input data
 * @param {Parser} parser -- The parser decorated
 * @param {IMaybe<number>} atLeast -- Number of at least needed
 * @param {IMaybe<number>} almost -- Number of at most accepted
 * @param {boolean} openLowerBound -- Modifier on lower bound if not defined:
 *                                  true : {None,Some(x)}
 *                                  false: {Some(x)}
 */
export const repeat = (parser: Parser, atLeast: IMaybe<number>, almost: IMaybe<number>, openLowerBound: boolean = false) : Parser => {
    const inner = (val: IStream) : ParserResult => {

        let parsedValues = [];
        let remain = val;
        let result : ParserResult;
        let repetitionAchieved=0;

        const parserRemain = (result: ParserResult) => result.isOk()
            ? result.unwrap().remain
            : result.unwrapError().remain

        const returnResult = (result: ParserResult, predicate: () => boolean) => {
            return predicate()
                ? Result.ok("", parserRemain(result))
                : Result.failed(result.unwrapError().error, val)
        }

        let finalResult = almost.match({
            some: (almost: number) => {

                for (repetitionAchieved; repetitionAchieved < almost; repetitionAchieved++) {

                    result = run(parser, remain);

                    if (!result.isOk()) {
                        break
                    }
                    remain = result.unwrap().remain
                    parsedValues = [...parsedValues, result.unwrap().parsed];

                }

                return atLeast.match({
                    some: (atLeast: number) => {
                        return returnResult(result, () => repetitionAchieved >= atLeast)
                    },
                    none: () => {

                        const predicate = !openLowerBound
                            ? (repetition, value) => repetition === value
                            : (repetition, value) => repetition <= value

                        return returnResult(result, () =>
                            predicate(repetitionAchieved, almost))

                    }
                })
            },
            none: () => {

                do {
                    result = run(parser, remain);

                    if (!result.isOk()) {
                        break
                    }
                    remain = result.unwrap().remain
                    parsedValues = [...parsedValues, result.unwrap().parsed];
                    repetitionAchieved++;

                } while (true);

                return atLeast.match({
                    some: (atLeast: number) => {

                       return returnResult(result, () =>
                           atLeast <= repetitionAchieved)
                    },
                    none: () => {

                       return Result.ok("", result.unwrapError().remain)
                    }
                });
            }
        })

        return finalResult.isOk()
            ? Result.ok(parsedValues, finalResult.unwrap().remain)
            : finalResult;
    }
    return buildParser(inner);
}

/**
 * Repeat zero or more time the parser
 * @param {Parser} parser -- Parser to repeat
 * @return {Parser} -- Parser repeated
 */
export const zeroOrMore = (parser: Parser) : Parser => {
    return repeat(parser, maybe(), maybe())
}

/**
 * Repeat at least one time a parser
 * @param {Parser} parser -- Parser to repeat
 * @return {Parser} -- Parser repeated
 */
export const oneOrMore = (parser: Parser) : Parser => {
    return repeat(parser, maybe(1), maybe())
}

/**
 * Apply a function on parser result
 * @param {Parser} parser -- Parser whose result must be mapped
 * @param {ParserResult => ParserResult} fn -- Mapping function
 * @return {Parser} -- Parser mapped
 */
export const map = (parser: Parser, fn: (r: ParserResult) => ParserResult) : Parser => {

    const inner = (val: IStream) : ParserResult => {

        return  fn(run(parser, val));
    }
    return buildParser(inner)
}