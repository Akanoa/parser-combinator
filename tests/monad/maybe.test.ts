import {maybe} from "../../src/monad/maybe";

describe("Maybe Monad", () => {

    let matchPattern =  {
        some: val => val + 12,
        none: () => 12
    }

    test("must apply the some pattern in match if value is defined", () => {
        let value = 12;
        let result = maybe(value).match(matchPattern)
        expect(result).toBe(24)
    });

    test("must apply the none pattern in match if value is not defined", () => {
        let result = maybe().match(matchPattern)
        expect(result).toBe(12)
    })

    test("should allow to check if monad is empty", () => {
        let maybeNone = maybe();
        expect(maybeNone.isNone()).toBeTruthy();
        let maybeSome = maybe(12);
        expect(maybeSome.isNone()).toBeFalsy();
    })
})