// Create an array containing a range of elements
export const range = (start: string, stop: string)  : Array<string> => {

    const result = [];
    let idx = start.charCodeAt(0), end = stop.charCodeAt(0);
    for (; idx <=end; ++idx){
        result.push(String.fromCharCode(idx));
    }
    return result;

}